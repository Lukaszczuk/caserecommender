Case Recommender - A Recommender Framework for Python
======================================================

Case Recommender is a Python implementation of a number of popular recommendation algorithms for both implicit and explicit feedback.  The framework aims to provide a rich set of components from which you can construct a customized recommender system from a set of algorithms. Case Recommender has different types of item recommendation and rating prediction approaches, and different metrics validation and evaluation.

Algorithms
^^^^^^^^^^^^

Item Recommendation:

- BPRMF

- ItemKNN

- Item Attribute KNN

- UserKNN

- User Attribute KNN

- Most Popular

- Random

Rating Prediction:

- Matrix Factorization (with and without baseline)

- SVD

- SVD++

- ItemKNN

- Item Attribute KNN

- UserKNN

- User Attribute KNN

- Item NSVD1 (with and without Batch)

- User NSVD1 (with and without Batch)


Evaluation and Validation Metrics
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- All-but-one Protocol

- Cross-fold- Validation

- Item Recommendation: Precision, Recall, NDCG and Map

- Rating Prediction: MAE and RMSE

- Statistical Analysis (T-test and Wilcoxon)

Requirements
^^^^^^^^^^^^^

- Python >= 3.5
- scipy
- numpy
- pandas
- scikit-learn

For Linux, Windows and MAC use:

    $ pip install requirements

For Windows libraries help use:

    http://www.lfd.uci.edu/~gohlke/pythonlibs/#matplotlib

Quick start
^^^^^^^^^^^^

Case Recommender can be installed using pip:

    $ pip install caserecommender