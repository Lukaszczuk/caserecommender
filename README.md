# Case Recommender - A Recommender Framework for Python
Case Recommender is a Python implementation of a number of popular recommendation algorithms for both implicit and explicit feedback.  The framework aims to provide a rich set of components from which you can construct a customized recommender system from a set of algorithms. Case Recommender has different types of item recommendation and rating prediction approaches, and different metrics validation and evaluation.

# Algorithms
Item Recommendation:

- BPRMF

- ItemKNN

- Item Attribute KNN

- UserKNN

- User Attribute KNN

- Most Popular

- Random

Rating Prediction:

- Matrix Factorization (with and without baseline)

- SVD

- SVD++

- ItemKNN

- Item Attribute KNN

- UserKNN

- User Attribute KNN

- Item NSVD1 (with and without Batch)

- User NSVD1 (with and without Batch)


# Evaluation and Validation Metrics

- All-but-one Protocol

- Cross-fold-Validation

- Item Recommendation: Precision, Recall, NDCG and Map

- Rating Prediction: MAE and RMSE

- Statistical Analysis (T-test and Wilcoxon)

# Requirements 

- Python >= 3.5
- scipy
- numpy 
- pandas
- scikit-learn

For Linux and MAC use:

    $ pip install requeriments

For Windows use:

    http://www.lfd.uci.edu/~gohlke/pythonlibs/#matplotlib
    

# Quick start

Case Recommender can be installed using pip:

    $ pip install caserecommender
    
# Usage

Divide Database (Fold Cross Validation)

    >> from caserec.utils.split_database import SplitDatabase
    >> SplitDatabase(input_file=dataset, dir_folds=dir_path, n_splits=10).kfoldcrossvalidation()             
              

Run Item Recommendation Algorithm (E.g: ItemKNN)

    >> from caserec.recommenders.item_recommendation.itemknn import ItemKNN
    >> ItemKNN(train_file, test_file).compute()
    
Run Rating Prediction Algorithm (E.g: ItemKNN)

    >> from caserec.recommenders.rating_prediction.itemknn import ItemKNN
    >> ItemKNN(train_file, test_file).compute()

Evaluate Ranking (Prec@N, Recall@N, NDCG@, Map@N and Map Total)

    >> from caserec.evaluation.item_recommendation import ItemRecommendationEvaluation
    >> ItemRecommendationEvaluation().evaluate_with_files(predictions_file, test_file)
    
Evaluate Ranking (MAE and RMSE)

    >> from caserec.evaluation.rating_prediction import RatingPredictionEvaluation
    >> RatingPredictionEvaluation().evaluate_with_files(predictions_file, test_file)


# Input

The input-files of traditional have to be placed in the corresponding subdirectory and are in csv-format with 3 columns 

- User

- Item

- Feedback
